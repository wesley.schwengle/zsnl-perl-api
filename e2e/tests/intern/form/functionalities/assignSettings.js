import {
    openPageAs
} from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import {
    goNext,
    assignSelf,
    assignCoworker,
    assignDepartment,
    getAssignmentType,
    getAssignmentDepartment,
    getAssignmentRole
} from './../../../../functions/common/form';

const assignSettings = [
    {
        type: 'both options disabled',
        casetype: 'Toewijzing bij registratie geen opties',
        assignOptionSelf: false,
        assignOptionCoworker: false,
        assignOptionDepartment: false
    },
    {
        type: 'assign to self option enabled',
        casetype: 'Toewijzing bij registratie mijzelf',
        assignOptionSelf: true,
        assignOptionCoworker: false,
        assignOptionDepartment: false,
        assignmentType: 'me'
    },
    {
        type: 'assignment option enabled',
        casetype: 'Toewijzing bij registratie andere',
        assignOptionSelf: true,
        assignOptionCoworker: true,
        assignOptionDepartment: true,
        assignmentType: 'org-unit',
        assignmentDepartment: '-Backoffice',
        assignmentRole: 'Behandelaar'
    },
    {
        type: 'assign to self and assignment options enabled',
        casetype: 'Toewijzing bij registratie alle opties',
        assignOptionSelf: true,
        assignOptionCoworker: true,
        assignOptionDepartment: true,
        assignmentType: 'me'
    }
];

assignSettings.forEach(assignSetting => {
    const { type, casetype, assignOptionSelf, assignOptionCoworker, assignOptionDepartment, assignmentDepartment, assignmentRole, assignmentType } = assignSetting;

    describe(`when starting a registration form with ${type}`, () => {
        beforeAll(() => {
            const data = {
                casetype: casetype,
                requestorType: 'citizen',
                requestorId: '1',
                channelOfContact: 'behandelaar'
            };

            openPageAs();
            startForm(data);
            goNext();
        });

        it('the correct assignment options should be present', () => {
            expect(assignSelf.isPresent()).toBe(assignOptionSelf);
            expect(assignCoworker.isPresent()).toBe(assignOptionCoworker);
            expect(assignDepartment.isPresent()).toBe(assignOptionDepartment);
        });

        if ( assignmentType ) {
            it(`the assignment type should be "${assignmentType}"`, () => {
                expect(getAssignmentType()).toEqual(assignmentType);
            });
        }

        if ( assignmentType === 'org-unit' ) {
            it(`the department should be "${assignmentDepartment}"`, () => {
                expect(getAssignmentDepartment()).toEqual(assignmentDepartment);
            });

            it(`the assignment should be "${assignmentRole}"`, () => {
                expect(getAssignmentRole()).toEqual(assignmentRole);
            });
        }
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
