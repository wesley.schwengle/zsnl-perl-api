DROP INDEX IF EXISTS object_data_case_type_status_idx;

CREATE INDEX CONCURRENTLY object_data_status_casetype_assignee_idx ON object_data ((index_hstore -> 'case.status'), ((index_hstore -> 'case.casetype.id')::numeric), ((index_hstore -> 'case.assignee.id')::numeric))
WHERE
    object_class = 'case' AND invalid IS FALSE;

