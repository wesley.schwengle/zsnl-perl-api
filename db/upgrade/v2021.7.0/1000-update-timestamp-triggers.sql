BEGIN;

    CREATE OR REPLACE FUNCTION insert_timestamps() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$
        BEGIN
            IF NEW.created is NULL
            THEN
                NEW.created = NOW();
            END IF;
            IF NEW.last_modified is NULL
            THEN
                NEW.last_modified = NOW();
            END IF;
            RETURN NEW;
        END;
    $$;

    CREATE OR REPLACE FUNCTION update_timestamps() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$
        BEGIN
            IF NEW.last_modified is NULL
            THEN
                NEW.last_modified = NOW();
            END IF;
            RETURN NEW;
        END;
    $$;

COMMIT;
