BEGIN;

    INSERT INTO queue (type, label, priority, metadata, data)
        SELECT 'touch_case', 'Devops: update cases without stalled_since', 3000, '{"require_object_model":1, "disable_acl": 1, "target":"backend"}',
            '{"case_object_id":"' || uuid || '"}'
            FROM object_data WHERE properties NOT LIKE '%stalled_since%' AND object_class = 'case';

COMMIT;


