BEGIN;
    ALTER TABLE custom_object_version ADD COLUMN external_reference TEXT;
    CREATE EXTENSION pg_trgm;
    CREATE INDEX ON custom_object_version USING GIN (external_reference gin_trgm_ops);
COMMIT;