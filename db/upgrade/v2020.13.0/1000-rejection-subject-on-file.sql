BEGIN;

  ALTER TABLE file ADD COLUMN rejected_by_display_name TEXT;

COMMIT;
