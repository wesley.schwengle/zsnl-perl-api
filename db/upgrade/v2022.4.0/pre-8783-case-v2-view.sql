BEGIN;

  DROP VIEW IF EXISTS VIEW_CASE_V2 CASCADE;
  
  CREATE VIEW view_case_v2 AS
      SELECT
          z.id,
          z.uuid,
          z.route_ou AS department,
          z.route_role,
          z.vernietigingsdatum AS destruction_date,
          z.pid AS number_parent,
          z.number_master,
          z.vervolg_van AS number_previous,
          z.onderwerp AS subject,
          z.onderwerp_extern AS subject_external,
          z.status,
          z.created AS date_created,
          z.last_modified AS date_modified,
          z.vernietigingsdatum AS date_destruction,
          z.afhandeldatum AS date_of_completion,
          z.registratiedatum AS date_of_registration,
          z.streefafhandeldatum AS date_target,
          z.html_email_template,
          z.payment_status,
          z.dutch_price AS price,
          z.contactkanaal AS channel_of_contact,
          z.archival_state,
          z.zaaktype_node_id,
          z.milestone AS milestone,
          get_confidential_mapping (z.confidentiality) AS confidentiality,
          CASE WHEN z.status = 'stalled'::text THEN
              zm.stalled_since
          ELSE
              NULL::timestamp WITHOUT time zone
          END AS stalled_since_date,
          CASE WHEN z.status = 'stalled'::text THEN
              z.stalled_until
          ELSE
              NULL::timestamp WITHOUT time zone
          END AS stalled_until_date,
          zm.current_deadline,
          zm.deadline_timeline,
          ztr.id AS result_id,
          ztr.resultaat AS result,
          ztr.selectielijst AS active_selection_list,
          CASE WHEN z.status = 'stalled'::text THEN
              zm.opschorten
          ELSE
              NULL::character varying
          END AS suspension_rationale,
          CASE WHEN z.status = 'resolved'::text THEN
              zm.afhandeling
          ELSE
              NULL::character varying
          END AS premature_completion_rationale,
          'Dossier'::text AS aggregation_scope,
      ztr.archiefnominatie AS type_of_archiving,
      rpt.label AS period_of_preservation,
      ztr.label AS result_description,
      ztr.comments AS result_explanation,
      ztr.properties::jsonb->'selectielijst_nummer' AS result_selection_list_number,
      ztr.properties::jsonb->'procestype_nummer' AS result_process_type_number,
      ztr.properties::jsonb->'procestype_naam' AS result_process_type_name,
      ztr.properties::jsonb->'procestype_omschrijving' AS result_process_type_description,
      ztr.properties::jsonb->'procestype_toelichting' AS result_process_type_explanation,
      ztr.properties::jsonb->'procestype_object' AS result_process_type_object,
      ztr.properties::jsonb->'procestype_generiek' AS result_process_type_generic,
      ztr.properties::jsonb->'herkomst'  AS result_origin,
      ztr.properties::jsonb->'procestermijn' AS result_process_term,
      z.streefafhandeldatum::date - COALESCE(z.afhandeldatum::date, NOW()::date) AS days_left,
      z.leadtime AS lead_time_real,
	  get_date_progress_from_case(hstore(z)) AS progress_days
      FROM zaak z
      LEFT JOIN zaak_meta zm ON zm.zaak_id = z.id
      LEFT JOIN zaaktype_resultaten ztr ON z.resultaat_id = ztr.id
      LEFT JOIN result_preservation_terms rpt ON ztr.bewaartermijn = rpt.code
      LEFT JOIN zaaktype_status zts ON z.zaaktype_node_id = zts.zaaktype_node_id
          AND zts.status = (z.milestone + 1)
      LEFT JOIN zaaktype_status zts_previous ON z.zaaktype_node_id = zts_previous.zaaktype_node_id
          AND zts_previous.status = z.milestone
	WHERE z.deleted IS NULL;

COMMIT;

