  INSERT INTO queue (type, label, priority, metadata, data)
    SELECT
    'touch_case_v0',
    'release: Update json attrs',
    950,
    -- metadata
    json_build_object(
      'require_object_model', 1,
      'disable_acl', 1,
      'target', 'backend'
    ),
    -- data
    json_build_object(
      'case_number', id
    )
  FROM zaak
  WHERE
    zaaktype_node_id in (
      SELECT
        zaaktype_node_id
      FROM
        zaaktype_kenmerken ztk
      JOIN
        bibliotheek_kenmerken bk
      ON
        ztk.bibliotheek_kenmerken_id = bk.id
      WHERE
        bk.value_type in (
          'geojson',
          'address_v2'
        )
      )
  ;



