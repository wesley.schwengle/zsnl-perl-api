BEGIN;
alter table case_relation add uuid uuid default uuid_generate_v4();
COMMIT;