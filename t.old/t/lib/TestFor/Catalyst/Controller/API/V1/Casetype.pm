package TestFor::Catalyst::Controller::API::V1::Casetype;
use base qw(ZSTest::Catalyst Zaaksysteem::API::V1::Roles::TestPreperation);

use TestSetup;

use Zaaksysteem::Types qw(UUID);

# use Zaaksysteem::TestMechanize;


=head1 NAME

TestFor::Catalyst::Controller:API::V1::Casetype - Proves the boundaries of our API: Casetype

=head1 SYNOPSIS

    See USAGE tests

    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/Casetype.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/casetype> namespace.

=head1 USAGE 

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 Retrieval

=cut

sub cat_api_v1_casetype_list : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $mech        = $zs->mech;
        $self->_create_api_interface;

        ### Build 3 casetypes
        $self->api_test_zaaktype for (1..3);

        ### Build another to work with
        my $casetype_data = $self->api_test_zaaktype;
        my $casetype      = $casetype_data->{casetype};

        $mech->zs_login;


        my $perl       = $mech->get_json('/api/v1/casetype');

        ### Verify the first casetype
        $self->validate_casetype_structure($perl, { set => 1 });
    });
}

=head3 Get a single casetype

=cut

sub cat_api_v1_casetype_get : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;
        $self->_create_api_interface;

        my $subject       = $zs->create_natuurlijk_persoon_ok;

        ### Build 3 casetypes
        my $casetype_data = $self->api_test_zaaktype({ preset_client => 'betrokkene-natuurlijk_persoon-' . $subject->id });

        my $casetype      = $casetype_data->{casetype};

        $mech->zs_login;

        ok($casetype->_object->uuid, 'Got casetype uuid');

        my $perl    = $mech->get_json('/api/v1/casetype/' . $casetype->_object->uuid);
        my $c_json  = $self->validate_casetype_structure($perl, {got_preset_client => 1});

        is($c_json->{id}, $casetype->_object->uuid, 'UUID of get matches with retrieved object: ' . $c_json->{id});
    });
}


=head2 validate_casetype_structure

Make sure we always retrieve a valid response

B<Testing the following information>

=cut

my @MINIMAL_CASETYPE_KEYS = qw/
    phases
    properties
    results
    sources
    title
    trigger
    subject_types
    preset_client
/;

sub validate_casetype_structure {
    my $self    = shift;
    my $perl    = shift;
    my $opts    = shift || {};

    $self->validate_zsapi_structure($perl);

    ### First case:
    my $casetype    = $perl->{result}->{instance};
    if ($opts->{set}) {
        is($perl->{result}->{type}, 'set', 'Got a set of data, multiple results');
        ok($perl->{result}->{instance}->{$_}, 'Got a set of data, column: ' . $_) for qw/rows pager/;
        ok($perl->{result}->{instance}->{pager}, 'Got a set of pager, column: ' . $_) for qw/next page pages prev rows total_rows/;
        isa_ok($perl->{result}->{instance}->{rows}, 'ARRAY', 'Got a set of data, got an array of rows');

        $casetype = $perl->{result}->{instance}->{rows}->[0]->{instance};
    }

    ok(exists $casetype->{$_}, "Found the required key: $_") for @MINIMAL_CASETYPE_KEYS;

    ok(UUID->check($casetype->{id}), 'Got a "uuid" casetype id');

    is($casetype->{trigger}, 'extern', 'Got external trigger');
    isa_ok($casetype->{phases}, 'ARRAY', 'Got a list of phases');
    isa_ok($casetype->{properties}, 'HASH', 'Got a hashref of properties');
    isa_ok($casetype->{sources}, 'ARRAY', 'Got a list of sources');
    isa_ok($casetype->{results}, 'ARRAY', 'Got a list of results');
    isa_ok($casetype->{subject_types}, 'ARRAY', 'Got a list of subject types');
    ok(@{ $casetype->{phases} } >= 2, 'Got a minimum of two phases');
    ok(@{ $casetype->{sources} } >= 1, 'Got a minimum of one source, like "webformulier" or "behandelaar"');
    ok(@{ $casetype->{results} } >= 1, 'Got a minimum of one result');

    for my $result (@{ $casetype->{results} }) {
        ok($result->{$_}, "Got filled result attribute: $_") for qw/
            archive_procedure
            comments
            label
            period_of_preservation
            selection_list
            trigger_archival
            type
            type_of_archiving
            type_of_dossier
        /;

        ok(exists $result->{$_}, "Got existing result attribute: $_") for qw/external_reference/;
    }

    if ($opts->{got_preset_client}) {

        isnt($casetype->{preset_client}{instance}, undef, "has preset client");
        isnt($casetype->{preset_client}{instance}{betrokkene_id}, undef, "has preset client id");
        isnt($casetype->{preset_client}{instance}{name}, undef, "has preset client name");

        is_deeply($casetype->{subject_types}, [qw/preset_client/], 'Found subject types: natuurlijk_persoon and niet_natuurlijk_persoon');
    } else {
        is_deeply($casetype->{subject_types}, [qw/natuurlijk_persoon niet_natuurlijk_persoon/], 'Found subject types: natuurlijk_persoon and niet_natuurlijk_persoon');
        is($casetype->{preset_client}{instance}, undef, "has preset client");
    }

    ok( $casetype->{legacy}->{zaaktype_node_id}, 'Legacy zaaktype_node_id');
    ok( $casetype->{legacy}->{zaaktype_id}, 'Legacy zaaktype_id');
    ok( exists $casetype->{queue_coworker_changes}, 'Got toplevel attribute: "queue_coworker_changes"');

    ### Check phases
    for my $phase ( @{ $casetype->{phases} }) {
        ok($phase->{name}, 'Got a "name" for this phase, called: ' . $phase->{name});
        ok($phase->{seq}, 'Got a sort order "seq" for this phase');
        exists($phase->{locked}), 'Got a "registration_phase_locked" parameter';

        if ($phase->{seq} == 1) {
            ok($phase->{locked}, 'Got a locked "registration_phase"');
        } else {
            ok(!$phase->{locked}, 'Got unlocked phase');
        }
        ok($phase->{id}, "Got a phase id");
        isa_ok($phase->{fields}, 'ARRAY', 'Got a list of fields for phase: ' . $phase->{name});

        for my $field (@{ $phase->{fields} }) {
            if (exists $field->{catalogue_id}) {
                ok($field->{id}, 'Got an ID as attribute identifier for this field: ' . $field->{id});
                ok($field->{catalogue_id}, 'Got an ID as catalogue identifier for this field: ' . $field->{catalogue_id});
                ok($field->{magic_string}, 'Got a magic string for this field: ' . $field->{magic_string});
                ok($field->{label}, 'Got a label for this field: ' . $field->{label} . ', field: ' . $field->{id});
                ok($field->{original_label}, 'Got an "original label: for this field: ' . $field->{original_label} . ', field: ' . $field->{id});
                ok($field->{type}, 'Got a "type" for this field: ' . $field->{type} . ', field: ' . $field->{id});
                ok(defined $field->{required}, 'Got a "required" for this field: ' . ($field->{required} ? 'true' : 'false') . ', field: ' . $field->{id});
                ok(defined $field->{is_system}, 'Got a "is_system" for this field: ' . ($field->{is_system} ? 'true' : 'false') . ', field: ' . $field->{id});
                ok(defined $field->{referential}, 'Got a "is_system" for this field: ' . ($field->{is_system} ? 'true' : 'false') . ', field: ' . $field->{id});
            }
            elsif(exists $field->{object_id}) {
                ok($field->{id}, 'Got an ID as attribute identifier for this field: ' . $field->{id});
                ok($field->{label}, 'Got a label for this field: ' . $field->{label});
                ok($field->{type}, 'Type of object field is "object"');
                is($field->{is_group}, 0, 'Object field is not a group');
                ok(
                    UUID->check($field->{object_id}),
                    'Got a UUID as attribute identifier: ' . $field->{object_id},
                );
                cmp_deeply(
                    $field->{object_metadata},
                    { 'some_metadata' => JSON::XS::true },
                    'Object metadata is returned correctly',
                );
            }
        }
    }

    ### Validate given attributes
    return $casetype;
}



1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
