package TestFor::Catalyst::Controller::API::Bulk::File;
use base qw(ZSTest::Catalyst);

use Moose;
use TestSetup;

use JSON;

sub bulk_file_tests : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;

        my $case = $zs->create_case_ok();
        my $f1 = $zs->create_file_ok(
            name => 'foo1.txt',
            db_params => {case => $case}
        );
        my $f2 = $zs->create_file_ok(
            name => 'foo2.txt',
            db_params => {case => $case}
        );

        $mech->zs_login;

        {
            $mech->post_json(
                $mech->zs_url_base . '/api/bulk/file/update',
                {},
            );
            my $data = _decode_json($mech->content);
            is_deeply($data->{result}, [], 'Empty action list leads to empty result list');
        }

        {
            $mech->post_json(
                $mech->zs_url_base . '/api/bulk/file/update',
                {
                    files => {
                        $f1->id => {
                            action => 'update_properties',
                            data   => { name => 'foo3.txt' },
                        },
                        $f2->id => {
                            action => 'no_such_action',
                            data   => { name => 'foo3.txt' },
                        },
                    },
                },
            );
            my $data = _decode_json($mech->content);
            is(scalar @{$data->{result}}, 2, '2 actions -- 2 results');
            for my $res (@{$data->{result}}) {
                is_deeply(
                    [sort keys %$res],
                    ['action', 'data', 'file_id', 'result'],
                    'Every result has correct key layout'
                );

                if ($res->{result} eq 'error') {
                    is($res->{action}, 'no_such_action', "Correct action errored");
                    is($res->{data}{status_code}, 500, "Error code returned");
                }
                elsif ($res->{result} eq 'success') {
                    is($res->{action}, 'update_properties', "Correct action succeeded");
                    ok(
                        exists($res->{data}{filestore_id}),
                        "The resulting object has a filestore_id (so it's probably a file object)"
                    );
                }
            }
        }
    });
}

sub _decode_json {
    my $json    = shift;
    my $jo      = JSON->new->utf8->pretty->canonical;
    return $jo->decode($json);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
