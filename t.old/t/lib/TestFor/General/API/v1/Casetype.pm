package TestFor::General::API::v1::Casetype;
use base qw(ZSTest);

use TestSetup;

use Zaaksysteem::API::v1::CasetypeACL;

sub get_casetype_users : Tests {
    $zs->txn_ok(
        sub {

            my %casetypes = (
                trusted => {
                    groups       => [3], # devops
                    roles        => [3, 1], # admin, ztb
                    confidential => 1,
                },
                public => {
                    groups => [3,  4], # Kantine, frontoffice
                    roles  => [12] # behandelaar
                },
                search => {
                    groups => [6],
                    roles  => [12] # behandelaar
                },
                nothing => {
                    groups => [0, 0],
                    roles  => [0, 0]
                },
                inactive => {
                    groups   => [4],
                    roles    => [12],
                    inactive => 1,
                },
            );

            foreach (keys %casetypes) {
                # if not nothing or inactive
                unless ($_ eq 'nothing' || $_ eq 'inactive') {
                    $casetypes{$_}{medewerker} = $zs->create_medewerker_ok(
                        username  => "$_ username",
                        group_ids => $casetypes{$_}{groups},
                        role_ids  => $casetypes{$_}{roles},
                    );
                }

                my $ct = $zs->create_zaaktype_ok;

                if ($_ eq 'search') {
                    $zs->create_zaaktype_authorisation_ok(
                        zaaktype => $ct,
                        recht    => 'zaak_search',
                        role_id  => $casetypes{$_}{roles}[-1],
                        group_id => $casetypes{$_}{groups}[-1],
                        confidential => $casetypes{$_}{confidential} // 0,
                    );
                }
                else {
                    $zs->create_zaaktype_authorisation_ok(
                        zaaktype => $ct,
                        role_id  => $casetypes{$_}{roles}[-1],
                        ou_id    => $casetypes{$_}{groups}[-1],
                        confidential => $casetypes{$_}{confidential} // 0,
                    );
                }

                if ($casetypes{$_}{inactive}) {
                    $ct->active(0);
                }

                $ct->update()->discard_changes;
                $ct->_sync_object($zs->object_model);
                $casetypes{$_}{casetype} = $ct;

            }

            my $ct_search = Zaaksysteem::API::v1::CasetypeACL->new(
                schema => $zs->schema
            );

            {
                my $rv = $ct_search->get_casetype_users;
                is(@$rv, (scalar keys %casetypes) -2, "All casetypes found");
            }

            foreach (keys %casetypes) {
                my $rv = $ct_search->get_casetype_users({casetype_uuid => $casetypes{$_}{casetype}->uuid});
                if ($_ eq 'search' || $_ eq 'nothing') {
                    is(@$rv, 0, "Found no results for $_");
                    next;
                }



                is(@$rv, 1, "Found one result for $_");
                my $acl = $rv->[0];

                if ($_ eq 'inactive') {
                    is(
                        $acl->public->{read}[0],
                        'gebruiker', "No username in public found for $_",
                    );
                    is(
                        $acl->trusted->{read}[0],
                        undef, "No username in public found for $_",
                    );
                    next;
                }

                is(
                    $acl->$_->{read}[-1],
                    $casetypes{$_}{medewerker}->subject_id->username,
                    'username matches',
                );

                my $opposite = $_ eq 'public' ? 'trusted' : 'public';
                is(
                    $acl->$opposite->{read}[0],
                    undef,
                    "No username in public found for $_",
                );

            }

            my $inactive = $ct_search->get_casetype_users({casetype_active => 0});
            is(@$inactive, 1, "Found one result for 'inactive'");

            my $active = $ct_search->get_casetype_users({casetype_active => 1});
            is(@$active, (scalar keys %casetypes) -3, "Found all results for 'active'");
        },
        "get_casetype_users",
    );
}

1;

__END__

=head1 NAME

TestFor::General::API::v1::Casetype - Test casetype ACL's

=head1 DESCRIPTION

    ./zs_prove -v t/lib/TestFor/General/API/v1/Casetype.pm

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
