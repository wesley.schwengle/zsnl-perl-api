// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular
    .module('Zaaksysteem.docs')
    .controller('nl.mintlab.docs.FileIntegrityController', [
      '$scope',
      'smartHttp',
      function ($scope, smartHttp) {
        $scope.status = 'unverified';
        $scope.loading = false;

        $scope.verifyIntegrity = function () {
          $scope.loading = true;
          smartHttp
            .connect({
              url: '/file/verify/file_id/' + $scope.entity.id,
              method: 'GET',
            })
            .success(function (data) {
              $scope.loading = false;
              $scope.status = data.result ? 'verified' : 'error';
            })
            .error(function (/*data*/) {
              $scope.status = 'error';
              $scope.loading = false;
            });
        };
      },
    ]);
})();
