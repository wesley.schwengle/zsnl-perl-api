// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  function TimelineEvent() {}

  TimelineEvent.prototype.getMimetype = function () {
    var mimetype = this.mimetype || 'Unknown';
    mimetype = mimetype.replace('application/', '').replace('image/', '');
    return mimetype;
  };

  window.zsDefine('nl.mintlab.timeline.TimelineEvent', function () {
    return TimelineEvent;
  });
})();
