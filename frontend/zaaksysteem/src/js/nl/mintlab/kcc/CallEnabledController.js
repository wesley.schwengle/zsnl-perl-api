// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.kcc')
    .controller('nl.mintlab.kcc.CallEnabledController', [
      '$scope',
      'callService',
      function ($scope, callService) {
        var safeApply = window.zsFetch('nl.mintlab.utils.safeApply');

        $scope.toggleEnabled = function () {
          if (callService.isEnabled()) {
            callService.disable();
          } else {
            callService.enable();
          }
        };

        $scope.isEnabled = function () {
          return callService.isEnabled();
        };

        $scope.isAvailable = function () {
          return callService.isAvailable();
        };

        $scope.$on('call.enabled.change', function () {
          safeApply($scope);
        });

        $scope.$on('call.available.change', function () {
          safeApply($scope);
        });
      },
    ]);
})();
