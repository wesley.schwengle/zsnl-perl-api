// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.form').directive('zsFormFieldCheckbox', [
    function () {
      return {
        require: 'ngModel',
        scope: true,
        link: function (scope, element, attrs, ngModel) {
          function setViewValue() {
            var val = scope.val,
              trueValue =
                attrs.zsTrueValue === undefined
                  ? true
                  : scope.$eval(attrs.zsTrueValue),
              falseValue =
                attrs.zsFalseValue === undefined
                  ? false
                  : scope.$eval(attrs.zsFalseValue);

            ngModel.$setViewValue(val ? trueValue : falseValue);
          }

          function setChildValue(val) {
            var viewValue = val,
              trueValue =
                attrs.zsTrueValue === undefined
                  ? true
                  : scope.$eval(attrs.zsTrueValue);

            scope.val = !!viewValue === !!trueValue;

            return scope.val;
          }

          scope.$on('form.change.committed', function (/*event, field*/) {
            setViewValue();
          });

          ngModel.$formatters.push(function (val) {
            setChildValue(val);
            return val;
          });
        },
      };
    },
  ]);
})();
