// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.admin')
    .controller('nl.mintlab.admin.EditRelationController', [
      '$scope',
      function ($scope) {
        $scope.init = function () {
          var accordionRelations = $(document).find('#accordion_relations');

          accordionRelations.accordion({
            collapsible: true,
            active: 0,
            animate: 100,
            heightStyle: 'content',
          });
        };
      },
    ]);
})();
