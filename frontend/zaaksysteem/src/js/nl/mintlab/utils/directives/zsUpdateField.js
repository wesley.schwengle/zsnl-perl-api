// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').directive('zsUpdateField', [
    'smartHttp',
    function (smartHttp) {
      function updateCalendarField(attrs, nwVal) {
        smartHttp
          .connect({
            method: 'POST',
            url: '/pip/zaak/' + attrs.zsCaseId + '/update_calendar_field',
            data: {
              value: nwVal,
              bibliotheek_kenmerken_id: attrs.name.match(/\d+$/)[0],
            },
          })
          .success(function (/*response*/) {})
          .error(function (/*response*/) {
            // some emission would be in order
          });
      }

      return {
        scope: false,
        link: function (scope, element, attrs) {
          scope.$watch(attrs.ngModel, function (nwVal, oldVal) {
            if (nwVal !== oldVal) {
              // since calendar is connected to an external server, it needs
              // to be treated differently
              // see qmatic.tt, all the way to the bottom. variables
              // are added as attributes to the input box.
              if (attrs.zsPip && attrs['class'] === 'veldoptie_calendar') {
                updateCalendarField(attrs, nwVal);
              } else {
                updateField($(element[0]), null, attrs.name);
              }
            }
          });
        },
      };
    },
  ]);
})();
