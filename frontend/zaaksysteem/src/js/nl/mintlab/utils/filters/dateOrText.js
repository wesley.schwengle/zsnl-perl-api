// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').factory('dateOrTextFilter', [
    '$filter',
    function ($filter) {
      var dateFilter = $filter('date');

      return function (string) {
        var date = new Date(string),
          parsed;
        if (!isNaN(date.getTime())) {
          parsed = dateFilter.apply(this, arguments);
        } else {
          parsed = string;
        }

        return parsed;
      };
    },
  ]);
})();
