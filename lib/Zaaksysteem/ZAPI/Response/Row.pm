package Zaaksysteem::ZAPI::Response::Row;

use Moose::Role;

sub from_row {
    my $self        = shift;
    my $row         = shift;

    die('Not a valid Row: ' . ref($row))
        unless (
            UNIVERSAL::isa($row, 'DBIx::Class') &&
            !UNIVERSAL::isa($row, 'DBIx::Class::ResultSet')
        );


    $self->_input_type('row');

    my $pager       = Data::Page->new();

    $pager->total_entries   (1);
    $pager->entries_per_page(1);
    $pager->current_page    (1);

    $self->_generate_paging_attributes(
        $self->pager($pager),
    );

    $self->result([ $row ]);

    $self->_validate_response;

    return $self->response;
}

around from_unknown => sub {
    my $orig        = shift;
    my $self        = shift;
    my ($data)      = @_;

    if (
        UNIVERSAL::isa($data, 'DBIx::Class') &&
        !UNIVERSAL::isa($data, 'DBIx::Class::ResultSet')
    ) {
        $self->from_row(@_);
    }

    $self->$orig( @_ );
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 from_row

TODO: Fix the POD

=cut

