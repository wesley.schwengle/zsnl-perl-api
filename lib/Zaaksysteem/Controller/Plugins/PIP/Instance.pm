package Zaaksysteem::Controller::Plugins::PIP::Instance;

use Moose;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::Plugins::PIP::Instance - API calls for PIP subjects

=head1 DESCRIPTION

=head1 ACTIONS

=head2 instances

This action will return all C<CustomerD> objects, where the owner is the
currently logged in PIP-subject.

=cut

sub instances : Chained('/plugins/pip/base') : PathPart('get_instances') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ zapi } = [
        {
            customer_config    => [ $c->model('Object')->search('customer_config', { customer_id => $c->session->{ pip }{ ztc_aanvrager }}) ],
            customer_d         => [ $c->model('Object')->search('customer_d', { owner => $c->session->{ pip }{ ztc_aanvrager }}) ]
        }
    ];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
