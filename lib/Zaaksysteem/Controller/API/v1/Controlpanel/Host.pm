package Zaaksysteem::Controller::API::v1::Controlpanel::Host;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Controlpanel::Host - APIv1 controller for Controlpanel objects

=head1 DESCRIPTION

This is the controller API class for
C<api/v1/controlpanel/[CONTROLPANEL_UUID]/host>. Extensive documentation
about this API can be found in
L<Zaaksysteem::Manual::API::V1::Controlpanel::Host>.

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Controlpanel::Host>

=cut

use BTTW::Tools;
use Zaaksysteem::Object::Types::Host;
use Zaaksysteem::Types qw(
    CustomerTemplateCurrent
    NonEmptyStr
    UUID
    ZSFQDN
);

sub BUILD {
    my $self = shift;

    $self->add_api_control_module_type('controlpanel');
    $self->add_api_context_permission('extern', 'allow_pip');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/controlpanel/host> routing namespace.

=cut

sub base : Chained('/api/v1/controlpanel/instance_base') : PathPart('host') : CaptureArgs(0) {
    my ($self, $c)      = @_;

    my $zql             = 'SELECT {} FROM host where owner="' . $c->stash->{controlpanel}->get_object_attribute('owner')->value . '"';

    $c->stash->{zql}    = Zaaksysteem::Search::ZQL->new($zql);

    my $set = try {
        return Zaaksysteem::API::v1::Set->new(
            iterator => $c->model('Object')->zql_search($zql)
        )->init_paging($c->request);
    } catch {
        $c->log->warn($_);

        throw(
            'api/v1/controlpanel/host',
            'API configuration error, unable to continue.'
        );
    };

    $c->stash->{ host_set }     = $set;
    $c->stash->{ hosts }        = $set->build_iterator->rs;
}

=head2 list

=head3 URL Path

C</api/v1/controlpanel/ID/host>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ host_set };
}

=head2 instance_base

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    # Retrieve case via Object model so we can benefit from builtin ACL stuff
    $c->stash->{ host } = try {
        return $c->stash->{ hosts }->find($uuid);
    } catch {
        $c->log->warn($_);

        throw('api/v1/controlpanel/host/not_found', sprintf(
            "The controlpanel host object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    };

    unless (defined $c->stash->{ host } && $c->stash->{ host }->object_class eq 'host') {
        throw('api/v1/controlpanel/host/not_found', sprintf(
            "The controlpanel host object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }
}

=head2 get

=head3 URL Path

C</api/v1/controlpanel/[CONTROLPANEL_UUID]/host/[HOST_UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ host };
}


=head2 create

=head3 URL Path

C</api/v1/controlpanel/[CONTROLPANEL_UUID]/host/create>

=cut

define_profile create => (
    required => {
        label => 'Str',
    },
    optional => {
        ssl_key  => 'Str',
        ssl_cert => 'Str',
        instance => UUID,
        template => CustomerTemplateCurrent,
    },
    require_some => {
        fqdn => [1, qw/fqdn/],
    },
    constraint_methods => {
        fqdn    => sub {
            my ($dfv, $val)     = @_;
            return 1 if ZSFQDN->check($val);
            $dfv->{_custom_messages}->{fqdn} = 'Hostname given for "%s" is reserved';
        },
    },
    field_filters => {
        fqdn     => ['lc'],
        template => ['lc'],
    },
    msgs    => sub {
        my $dfv     = shift;
        my $rv      = {};

        return $rv unless $dfv->{_custom_messages};

        for my $key (keys %{ $dfv->{_custom_messages} }) {
            $rv->{$key} = $dfv->{_custom_messages}->{$key};
        }

        return $rv;
    }
);

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    my $params  = assert_profile($c->req->params)->valid;

    ### Validate if owner already has an object
    if ($c->model('Object')->count('Host', { fqdn => $params->{fqdn} })) {
        throw(
            'api/v1/controlpanel/host/fqdn_exists',
            'controlpanel host already exists with given name'
        );
    }

    # Empty string === no template
    if (exists $params->{template} && !NonEmptyStr->check($params->{template})) {
        delete $params->{template};
    }

    my $host;
    try {
        $c->model('DB')->txn_do(sub {
            $host = $self->_create_host_object(
                $c->model('Object'),
                {
                    controlpanel        => $c->stash->{controlpanel},
                    %$params
                },
                $c
            );
        });
    } catch {
        throw('api/v1/controlpanel/host/fault', sprintf(
            'There was a problem creating this controlpanel host object: %s',
            $_
        ));
    };

    $c->stash->{result} = $c->model('DB::ObjectData')->find({ uuid => $host->id });
}

=head2 update

=head3 URL Path

C</api/v1/controlpanel/[CONTROLPANEL_UUID]/host/[HOST_UUID]/update>

=cut

define_profile update => (
    missing_optional_valid => 1,
    optional => {
        label      => 'Str',
        ssl_cert   => 'Str',
        ssl_key    => 'Str',
        instance   => UUID,
        template   => CustomerTemplateCurrent,
    },
    require_some => {
        fqdn     => [1, qw/fqdn/],
    },
    constraint_methods => {
        fqdn    => sub {
            my ($dfv, $val)     = @_;
            return 1 if ZSFQDN->check($val);
            $dfv->{_custom_messages}->{fqdn} = 'Hostname given for "%s" is reserved';
        },
    },
    field_filters => {
        fqdn    => ['lc']
    },
    msgs    => sub {
        my $dfv     = shift;
        my $rv      = {};

        return $rv unless $dfv->{_custom_messages};

        for my $key (keys %{ $dfv->{_custom_messages} }) {
            $rv->{$key} = $dfv->{_custom_messages}->{$key};
        }

        return $rv;
    }
);

sub update : Chained('instance_base') : PathPart('update') : Args(0) : RW {
    my ($self, $c) = @_;

    my $params  = assert_profile($c->req->params)->valid;

    if (keys %$params) {
        try {
            $c->model('DB')->txn_do(sub {
                $self->_update_host_object($c->model('Object'), $c->stash->{host}, $params);
            });
        } catch {
            throw('api/v1/controlpanel/host/fault', sprintf(
                'There was a problem updating this controlpanel host object: %s',
                $_
            ));
        };
    }

    $c->stash->{result} = $c->model('DB::ObjectData')->find({ uuid => $c->stash->{host}->uuid });
}

sig _update_host_object => 'Zaaksysteem::Object::Model, Zaaksysteem::Model::DB::ObjectData,?HashRef';

sub _update_host_object {
    my ($self, $objectmodel, $host, $params) = @_;

    my $object = $objectmodel->retrieve(uuid => $host->id);

    ###
    ### Manage related instances
    ###
    if (exists $params->{instance}) {
        $object->set_instance($objectmodel, $params->{instance});
        delete($params->{instance});
    }

    foreach my $name (keys %$params) {
        my $attr = $object->meta->find_attribute_by_name($name);

        if (!$attr) {
            throw('host/update/attribute/not_found', "Unable to find attribute by name $name");
        }

        # Only clear attributes when the params is undef and/or has no
        # lenght and the attribute has a value and we actually defined a
        # clearer. Otherwise set the attributes to whatever was given to
        # us.
        if (!length($params->{$name} // '') && $attr->has_clearer($object)) {
            $attr->clear_value($object) if $attr->has_value($object);
        }
        else {
            $object->$name($params->{$name});
        }

    }

    return $objectmodel->save(object => $object);
}

define_profile _create_host_object => (
    required => {
        controlpanel => 'Zaaksysteem::Backend::Object::Data::Component',
        fqdn         => ZSFQDN,
        label        => 'Str',
    },
    optional => { ssl_key => 'Str', ssl_cert => 'Str', instance => UUID, },
);

sub _create_host_object {
    my $self                    = shift;
    my $objectmodel             = shift;

    my $params                  = assert_profile(shift || {})->valid;
    my $c                       = shift;

    my $host                    = Zaaksysteem::Object::Types::Host->new(
        owner    => $params->{controlpanel}->get_object_attribute('owner')->value,
        %$params
    );

    ### Every controlpanel gets a default ACL for behandelaar
    my ($behandelaar) = grep ({ $_->system_role && $_->name eq 'Behandelaar' } @{ $c->model('DB::Roles')->get_all_cached($c->stash) });
    $host->permit($behandelaar, qw/read write/);

    if (exists $params->{instance}) {
        $host->set_instance($objectmodel, $params->{instance});
    }

    $objectmodel->save(
        object => $host
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
