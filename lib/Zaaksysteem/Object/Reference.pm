package Zaaksysteem::Object::Reference;

use Moose::Role;

use BTTW::Tools;
use Zaaksysteem::Types qw[UUID];

requires qw[_instantiable _instance];

=head1 NAME

Zaaksysteem::Object::Reference - Moose role with behavior for objects with
an identity

=head1 DESCRIPTION

=head1 REQUIRED INTERFACES

=head2 _instantiable

Consuming classes must implement the C<_instantiable> method, which return
value will be used to determine if the reference instance is capable of
producing the referent instance (thing being pointed to).

=head2 _instance

Consuming classes must implement the C<_instance> method, which must return
an instance of L<Zaaksysteem::Object> when called. This method will not be
called unless L</_instantiable> returns true-ish.

=cut

sig _instance => '=> Zaaksysteem::Object';

=head1 ATTRIBUTES

=head2 id

This attribute may hold a L<UUID|Zaaksysteem::Types/UUID> value, uniquely
identifying the instance this role is applied on.

Handles: C<has_id>, C<clear_id>.

=cut

has id => (
    is => 'rw',
    isa => UUID,
    predicate => 'has_id',
    clearer => 'clear_id'
);

=head1 METHODS

=head2 _ref

This convenience method returns a new
L<reference instance|/"Reference instance"> based on the data stored in the
current instance (C<$self>).

It may fail if the method is called on a plain object instance with no C<id>
value, since C<id> is a required field for the reference instance.

=cut

sub _ref {
    my $self = shift;

    return Zaaksysteem::Object::Reference::Instance->new(
        id => $self->id,
        type => $self->type,
        _stringification => $self->TO_STRING
    );
}

=head2 _as_string

Generic stringification method available to all consumers. Produces a short
string which can be used to identify the object by the last 6 hexchars of it's
L</id>. Returns strings like C<my_type(...1b2b3c)> or C<my_type(unsynched)>,
depending on the definedness of the L</id> attribute.

=cut

sub _as_string {
    my $self = shift;

    my $id = 'unsynched';

    if ($self->has_id) {
        $id = sprintf('...%s', substr($self->id, -6));
    }

    return sprintf('%s(%s)', $self->type, $id);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2018 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
