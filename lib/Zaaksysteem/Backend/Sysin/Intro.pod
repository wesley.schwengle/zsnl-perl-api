=head1 NAME

Zaaksysteem::Backend::Sysin::Intro - System Integration development Docs

=head1 SYNOPSIS

    my $result = $schema->resultset('Interface')->interface_create({
        name         => 'Test Interface',   ### Module label (db field)
        module       => 'csv',              ### Module name
        interface_config => {
            how_hot      => 'should pass',  ### CSV custom field (interface_config)
        }
    });

    my $result = $result->interface_update({
        interface_config => {
            how_hot      => 'other value',  ### CSV custom field (interface_config)
        }
    });


=head1 DESCRIPTION

Our SYSIN module provides an interface between the administrator of zaaksysteem.nl
and our different services.

=head1 COMPONENTS

The Sysin module consists of several components, which combined, form a complete
system integration system.

=head2 Modules

Using predefined modules, one can create different interfaces on Sysin. For more
information about writing a module, see L<Zaaksysteem::Backend::Sysin::Modules>

=head2 Interface

=head2 Transaction

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Controller::Sysin> L<Zaaksysteem>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
