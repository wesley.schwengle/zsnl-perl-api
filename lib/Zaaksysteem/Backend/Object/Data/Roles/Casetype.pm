package Zaaksysteem::Backend::Object::Data::Roles::Casetype;

use Moose::Role;

=head1 NAME

Zaaksysteem::Backend::Object::Data::Roles::Casetype

=head1 SYNOPSIS

    my $object = $model->retrieve(uuid => $uuid_of_casetype);

    my $zaaktype = $object->get_source_object;

=head1 DESCRIPTION

This class merely exists so other bits of code don't need to muck about
with databases and resultsets too much.

=head1 METHODS

=head2 get_source_object

This method overrides
L<Zaaksysteem::Backend::Object::Data::Component/get_source_object> with a
concrete implementation.

=cut

override get_source_object => sub {
    my $self = shift;
    
    return $self->result_source->schema->resultset('Zaaktype')->find(
        $self->object_id
    );
};

=head2 text_vector_terms

Overrides L<Zaaksysteem::Backend::Object::Data::Component/text_vector_terms>
so the method returns keywords relevant for casetype object instances.

This method will attempt to retrieve the values for the C<name> and
C<keywords> attributes.

=cut

override text_vector_terms => sub {
    my $self = shift;

    return unless $self->properties && exists $self->properties->{ values };

    return grep { defined }
            map { $self->get_object_attribute($_)->vectorize_value }
           grep { $self->has_object_attribute($_) }
                qw[name keywords];
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
