use utf8;
package Zaaksysteem::Schema::Rights;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Rights

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<rights>

=cut

__PACKAGE__->table("rights");

=head1 ACCESSORS

=head2 name

  data_type: 'text'
  is_nullable: 0

=head2 description

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "name",
  { data_type => "text", is_nullable => 0 },
  "description",
  { data_type => "text", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</name>

=back

=cut

__PACKAGE__->set_primary_key("name");

=head1 RELATIONS

=head2 role_rights

Type: has_many

Related object: L<Zaaksysteem::Schema::RoleRights>

=cut

__PACKAGE__->has_many(
  "role_rights",
  "Zaaksysteem::Schema::RoleRights",
  { "foreign.rights_name" => "self.name" },
  undef,
);

=head2 role_ids

Type: many_to_many

Composing rels: L</role_rights> -> role_id

=cut

__PACKAGE__->many_to_many("role_ids", "role_rights", "role_id");


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-10-11 09:39:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:hip3yZRgblXZxBYsRaLNjw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;


=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
