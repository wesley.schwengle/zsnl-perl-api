use utf8;
package Zaaksysteem::Schema::SubjectPositionMatrix;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::SubjectPositionMatrix

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<subject_position_matrix>

=cut

__PACKAGE__->table("subject_position_matrix");

=head1 ACCESSORS

=head2 subject_id

  data_type: 'integer'
  is_nullable: 1

=head2 group_id

  data_type: 'integer'
  is_nullable: 1

=head2 role_id

  data_type: 'integer'
  is_nullable: 1

=head2 position

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "subject_id",
  { data_type => "integer", is_nullable => 1 },
  "group_id",
  { data_type => "integer", is_nullable => 1 },
  "role_id",
  { data_type => "integer", is_nullable => 1 },
  "position",
  { data_type => "text", is_nullable => 1 },
);

=head1 UNIQUE CONSTRAINTS

=head2 C<subject_position_matrix_group_role_subject_idx>

=over 4

=item * L</subject_id>

=item * L</role_id>

=item * L</group_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "subject_position_matrix_group_role_subject_idx",
  ["subject_id", "role_id", "group_id"],
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-09-21 10:36:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:SzmF8SySYGxYPo6A2AASKg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
