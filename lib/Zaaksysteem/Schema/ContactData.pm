use utf8;
package Zaaksysteem::Schema::ContactData;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ContactData

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<contact_data>

=cut

__PACKAGE__->table("contact_data");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'contact_data_id_seq'

=head2 gegevens_magazijn_id

  data_type: 'integer'
  is_nullable: 1

=head2 betrokkene_type

  data_type: 'integer'
  is_nullable: 1

=head2 mobiel

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 telefoonnummer

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 email

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 note

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "contact_data_id_seq",
  },
  "gegevens_magazijn_id",
  { data_type => "integer", is_nullable => 1 },
  "betrokkene_type",
  { data_type => "integer", is_nullable => 1 },
  "mobiel",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "telefoonnummer",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "email",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "note",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<contact_data_betrokkene_type_gegevens_magazijn_id_idx>

=over 4

=item * L</betrokkene_type>

=item * L</gegevens_magazijn_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "contact_data_betrokkene_type_gegevens_magazijn_id_idx",
  ["betrokkene_type", "gegevens_magazijn_id"],
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2022-05-17 11:18:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:6iU/yNXWs/Qvw5CESOs1Ag

__PACKAGE__->resultset_class('Zaaksysteem::Betrokkene::ContactDataResultSet');

__PACKAGE__->load_components(
    '+Zaaksysteem::DB::Component::ContactData',
    __PACKAGE__->load_components()
);

# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

