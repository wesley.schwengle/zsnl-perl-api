[% veldoptietype = params.type -%]

[% BLOCK attr_checkbox_solo %]
<div class="column large-7">
    <input type="hidden" name="dialog_checkboxes" value="[% name %]" />
    <input type="checkbox"
           name="[% name %]"
           value="1"
           [% IF class %]
           class="[% class %]"
           [% END %]
           [% IF $param %]
           checked="checked"
           [% END %]/> [% description %]
</div>
[% END %]

[% BLOCK attr_checkbox %]
<div class="column large-5">[% title %]</div>
[% PROCESS attr_checkbox_solo %]
[% END %]

[% BLOCK attr_textbox %]
<div class="column large-5">[% title %]</div>
<div class="column large-7">
    <input
      type="text"
      name="[% name %]"
      value="[% $param | html_entity %]"
      class="input_large"
      [% IF placeholder %]
      placeholder="[% placeholder %]"
      [% END %]
    >
</div>

[% END %]

[% BLOCK show_single_attribute_field %]

[% name = 'kenmerken_custom_object_attr_' _ f.magic_string;
   key = f.magic_string
   value = params.properties.custom_object.attributes.$key;
%]
  <tr>
    <td>
      <div>
        <input type="text"
               name="[% f.magic_string | html_entity %]"
               value="[% f.label | html_entity %]" readonly />
      </div>
    </td>
    <td>
      <div>
        <input type="text"
               name="[% name | html_entity %]"
               placeholder="[[magic_string]]"
               value="[% value | html_entity %]"
         />
      </div>
    </td>
  </tr>

[% END %]

[% BLOCK show_attributes_and_value_mapping %]

[%
  fields = custom_object.custom_field_definition.custom_fields;

  FOREACH f IN fields;
    PROCESS show_single_attribute_field;
  END;
%]

[% END %]


<div id="kenmerk_definitie">
<form>
<input type="hidden" name="uniqueidr" value="[% c.req.params.uniqueidr %]">
<input type="hidden" name="update" value="1">
<input type="hidden" name="rownumber" value="[% rownumber %]">
<input type="hidden" name="edit_id" value="[% c.req.params.edit_id %]">
<div id="kenmerk_template" class="form">
        [% #IF milestone_number != 1 #- is this relevant for intake? %]
        <div class="row">
            <div class="column large-5">
                <div class="field-label field-has-help">
                    <div class="field-help">
                        [% PROCESS widgets/general/dropdown.tt
                            icon = 'icon-question-sign',
                            icon_type = 'icon-font-awesome',
                            content_overflow = 'dropdown-content-overflow',
                            dropdown_content = 'Als de zaak een hoofdzaak heeft worden waarden en instellingen van gelijknamig kenmerk in hoofdzaak gebruikt. Onderstaande instellingen worden gebruikt als de zaak niet gekoppeld is aan een hoofdzaak.',
                            allow_click = 1
                        %]
                    </div>
                    <label class="titel">Referentieel</label>
                </div>
            </div>
            [% PROCESS attr_checkbox_solo
              name = "kenmerken_referential"
              param = "params.referential"
            %]
        </div>
        [% #END %]

        <div class="row attribute_mandatory">
            [% PROCESS attr_checkbox
               title = "Verplicht"
               class = "kenmerken_value_mandatory"
               name  = "kenmerken_value_mandatory"
               param = "params.value_mandatory"
            %]
        </div>

        <div class="row attribute_system">
            [% PROCESS attr_checkbox
               title = "Gebruik als systeemkenmerk"
               class = "kenmerken_is_systeemkenmerk"
               name  = "kenmerken_is_systeemkenmerk"
               param = "params.is_systeemkenmerk"
            %]
        </div>

        [% value_type = bibliotheek_kenmerk.value_type; %]

        <div class="row">
            <div class="column large-5">Titel</div>
            <div class="column large-7">
                <input type="text" name="kenmerken_label" value="[% params.label %]" class="input_large">
            </div>
        </div>
        <div class="row">
            <div class="column large-5">Toelichting (intern)</div>
            <div class="column large-7">
                <input type="hidden" class="zs-rich-text-editor" value="[% params.help | html %]" features="bold italic underline link bullet ordered" name="kenmerken_help" />
            </div>
        </div>
        <div class="row [% IF milestone_number != 1 %]publish_pip[% END %]">
            <div class="column large-5">Toelichting (extern)</div>
            <div class="column large-7">
                <input type="hidden" class="zs-rich-text-editor" value="[% params.help_extern | html %]" features="bold italic underline link bullet ordered" name="kenmerken_help_extern" />
            </div>
        </div>

        [% IF bibliotheek_kenmerk.type_multiple && !(value_type == 'relationship' && bibliotheek_kenmerk.relationship_type == 'custom_object')%]
        <div class="row">
            <div class="column large-5">Label voor opplusbaar kenmerk</div>
            <div class="column large-7">
                <input type="text" name="kenmerken_label_multiple" value="[% params.label_multiple %]">
            </div>
        </div>
        [% END %]

        [% IF ZCONSTANTS.veld_opties.$veldoptietype.can_zaakadres %]
        <div class="row">
            <div class="column large-5">Gebruiken als zaakadres</div>
            <div class="column large-7">
                <input type="hidden" name="dialog_checkboxes" value="kenmerken_bag_zaakadres" />
                <input type="checkbox" name="kenmerken_bag_zaakadres" value="1"[% (params.bag_zaakadres ? ' checked' : '') %] />
            </div>
        </div>
        [% END %]

        [% IF value_type == 'relationship' &&
            bibliotheek_kenmerk.relationship_type == 'subject' %]
            <div class="row">
                <div class="column large-5">Rol</div>
                <div class="column large-7">
                    [% user_roles = c.get_user_roles %]
                    <select name="kenmerken_relationship_subject_role" id="kenmerken_relationship_subject_role">
                      [% FOREACH role IN user_roles %]
                        [% role_label = role.label | html_entity %]

                        <option value="[% role_label %]"
                          [% IF role_label == params.properties.relationship_subject_role %]
                          selected
                          [% END %]
                        >
                            [% role_label %]
                        </option>
                      [% END %]
                    </select>
                </div>
            </div>
        [% END %]

        [% IF ZCONSTANTS.veld_opties.$veldoptietype.show_on_map %]
        <div class="row">
            <div class="column large-5">Tonen op kaart</div>
            <div class="column large-7">
                <input type="hidden" name="dialog_checkboxes" value="kenmerken_show_on_map" />
                <input
                    type="checkbox"
                    name="kenmerken_show_on_map"
                    [% IF params.properties.show_on_map %]
                    checked="checked"
                    [% END %]
                />
            </div>
        </div>
        [% END %]

        <div class="row">
            <div class="column large-5">Publiceren</div>
            <div class="column large-7">
                <input type="hidden" name="dialog_checkboxes" value="kenmerken_pip" />
                <input type="checkbox"
                       name="kenmerken_pip"
                       class="kenmerken_pip"
                       [% IF params.pip %] checked="checked" [% END %]
                />
                  Persoonlijke Internet Pagina (PIP)
                <br/>
                <input type="hidden" name="dialog_checkboxes" value="kenmerken_publish_public" />
                <input type="checkbox" name="kenmerken_publish_public" value="1"[% (params.publish_public ? ' checked' : '') %] /> Extern systeem (API)
            </div>
        </div>
        <div class="row publish_pip">
            <div class="column large-5">Aanvrager/gemachtigde kan kenmerk wijzigen in PIP</div>
            <div class="column large-7">
                <input type="hidden" name="dialog_checkboxes" value="kenmerken_pip_can_change" />
                <input type="checkbox" name="kenmerken_pip_can_change" value="1"[% (params.pip_can_change ? ' checked' : '') %] />
            </div>
        </div>

        <div class="row" data-ng-controller="nl.mintlab.admin.RequiredPermissionController">

            <div class="column large-5">Specifieke behandelrechten</div>
            <div class="column large-7">
                <script type="text/zs-scope-data">[% params.required_permissions %]</script>

                <input type="hidden" value="<[getJson()]>" name="kenmerken_required_permissions"/>
                <div class="kenmerk-specific-rights">
                    <ul>
                        <li data-ng-repeat="selectedUnit in selectedUnits">
                            <span>Afdeling</span>
                            <select data-ng-model="selectedUnit.org_unit_id" data-ng-options="unit.org_unit_id as unit.name for unit in getUnits()" data-ng-change="handleUnitChange(selectedUnit)">
                            </select><br/>
                            <span>Rol</span>
                            <select data-ng-model="selectedUnit.role_id" data-ng-options="role.role_id as getRoleName(role) for role in getRolesForUnit(selectedUnit) | orderBy:'system'">
                            </select>
                            <button type="button" data-ng-click="remove(selectedUnit)">
                                <i class="icon-remove icon-font-awesome"></i>
                            </button>
                        </li>
                    </ul>

                    <button type="button" data-ng-click="add()" class="button button-secondary button-small">
                        Toevoegen
                    </button>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="column large-5">
                <div class="field-label field-has-help">
                    <div class="field-help">
                    [% PROCESS widgets/general/dropdown.tt
                        icon = 'icon-question-sign',
                        icon_type = 'icon-font-awesome',
                        content_overflow = 'dropdown-content-overflow',
                        dropdown_content = "Als deze optie aan staat, worden wijzigingen op dit kenmerk van gebruikers met specifieke behandelrechten niet in de wachtrij gezet maar direct uitgevoerd.",
                        allow_click = 1
                    %]
                    </div>
                    <label class="titel">Wachtrij overslaan</label>
                </div>
            </div>
            <div class="column large-7">
                <div>
                    <input type="hidden" name="dialog_checkboxes" value="kenmerken_skip_change_approval" />
                    <input type="checkbox" value="1" name="kenmerken_skip_change_approval"[% (params.properties.skip_change_approval ? ' checked="checked"' : '') %] />
                </div>
            </div>
        </div>

        [% IF bibliotheek_kenmerk.is_custom_object_relationship %]
          [% custom_object = bibliotheek_kenmerk.get_related_custom_object %]
          [% custom_object_name = custom_object.name %]
        <div class="row">

            [% PROCESS attr_checkbox
               title = "Object (" _ custom_object_name _ ") aanmaken"
               class = "custom_object_create"
               name  = "kenmerken_custom_object_create"
               param = "params.properties.custom_object.create.enabled"
            %]

            <div class="object_create">

              <div>
                [% PROCESS attr_textbox
                   title = "Label voor aanmaakknop"
                   name  = "kenmerken_custom_object_label_create"
                   param = "params.properties.custom_object.create.label"
                   placeholder = 'Aanmaken ' _ custom_object_name
                %]
              </div>

              <div>
                <div class="column large-5">Kenmerk mapping</div>
                <div class="column large-7">
                  <table id="kenmerk_template" class="form">
                  <tr>
                    <td>
                      <div>
                        Kenmerk van object
                      </div>
                    </td>
                    <td>
                      <div>
                        Vooringevulde waarde
                      </div>
                    </td>
                  </tr>
                  [% PROCESS show_attributes_and_value_mapping %]
                  </table>
                </div>
              </div>

            </div>
        </div>


        [% END %]

        [% IF value_type == 'geolatlon' || value_type == 'address_v2' %]
            <div class="row">
                <div class="column large-5">Kaartlaag</div>
                <div class="column large-7">
                    [% IF map_layer_settings %]
                    <select name="kenmerken_map_wms_layer_id">
                        <option value="">Geen kaartlaag</option>
                        [% FOR layer IN map_layer_settings.wms_layers %]
                        [% NEXT UNLESS layer.active %]
                        <option value="[% layer.layer_name %]"[% params.properties.map_wms_layer_id == layer.layer_name ? ' selected' : '' %]>
                            [% layer.label %]
                        </option>
                        [% END %]
                    </select>
                    [% ELSE %]
                    <span>
                        Er is geen actieve kaartconfiguratie ingesteld, kaartlaag
                        functionaliteit kan niet gebruikt worden
                    </span>
                    [% END %]
                </div>
            </div>

            [% IF value_type == 'geolatlon' && map_layer_settings %]
            <div class="row">
                <div class="column large-5">Gebruik als zaaklocatie</div>
                <div class="column large-7">
                    <input type="hidden" name="dialog_checkboxes" value="kenmerken_map_case_location" />
                    <input type="checkbox" value="1" name="kenmerken_map_case_location"[% params.properties.map_case_location ? ' checked' : '' %] />
                </div>
            </div>
            [% END %]

            [% IF map_layer_settings %]
            <div class="row">
                <div class="column large-5">WMS-Kaartlaag kenmerk</div>
                <div class="column large-7 ezra_objectsearch_kenmerk zoek_veld">
                    <input type="hidden" name="kenmerken_map_wms_feature_attribute_id" value="[% params.properties.map_wms_feature_attribute_id %]" />
                    <input type="text" name="kenmerken_map_wms_feature_attribute_label" value="[% params.properties.map_wms_feature_attribute_label %]" class="searchbox ezra_kenmerk_autocomplete" placeholder="Zoek kenmerk..." hidden-field="kenmerken_map_wms_feature_attribute_id" />
                </div>
            </div>
            [% END %]

        [% ELSIF value_type == 'date' && milestone_number == 1 %]
            <div class="row">
                <div class="column large-5">

                    <div class="field-label field-has-help">
                        <div class="field-help">
                        [% PROCESS widgets/general/dropdown.tt
                            icon = 'icon-question-sign',
                            icon_type = 'icon-font-awesome',
                            content_overflow = 'dropdown-content-overflow',
                            dropdown_content = 'De data die op formulieren geselecteerd kunnen worden in de kalender zijn beperkt tot de opgegeven periode.',
                            allow_click = 1
                        %]
                        </div>
                        <label class="titel">Datumbeperking</label>
                    </div>
                </div>
                <div class="column large-7">
                    [% PROCESS widgets/beheer/date_limitations.tt
                        info_box = "Dit is de infobox"
                        label    = "Start"
                        type     = "start"
                    %]

                    [% PROCESS widgets/beheer/date_limitations.tt
                        info_box = "Dit is de infobox"
                        label    = "Eindigt"
                        type     = "end"
                    %]
                </div>
            </div>
        [% END %]
        <div class="form-actions">
            <input type="submit" value="Opslaan" class="button button-primary">
        </div>
    </form>
</div>
