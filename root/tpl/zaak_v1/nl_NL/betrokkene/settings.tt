
[% IF ! show_medewerker_settings %]
  [% PROCESS betrokkene/alternative_authentication.tt %]
  [% RETURN %]
[% END %]

[% IF user_can_change_password %]
<div data-ng-controller="nl.mintlab.user.UserPasswordController" class="contact-wachtwoord-wijzigen contact-settings-block">

	<h3>Wachtwoord wijzigen</h3>
		
	<div data-zs-form-template-parser="<[passwordForm]>">
	</div>
	
	<script type="text/zs-scope-data">
		{
			"passwordForm": {
				"name": "changepass",
				"actions": [ 
					{
						"name": "submit",
						"type": "submit",
						"label": "Opslaan",
						"importance": "primary",
						"data": {
							"url": "/api/user/password"
						}
					}
				],
				"fieldsets": [
					{
						"name": "password-change",
						"fields": [
							{
								"name": "old_password",
								"label": "Huidig wachtwoord",
								"type": "password",
								"required": true
							},
							{
								"name": "new_password",
								"label": "Nieuw wachtwoord",
								"type": "password",
								"required": true,
								"data": {
									"showPasswordStrength": true
								}
							},
							{
								"name": "confirm_password",
								"label": "Bevestig wachtwoord",
								"type": "password",
								"required": true,
								"data": {
									"validation": "new_password==confirm_password"
								}
							}
						]
					}
					
				]
			}
		}
	</script>
</div>
[% END %]

[% IF is_own_dossier && user_can_change_notifications %]
<div data-ng-controller="nl.mintlab.user.UserNotificationsController" class="contact-telefonie-wijzigen contact-settings-block">
    <h3>Notificaties</h3>
    <span class="subtitle">Selecteer de gebeurtenissen waarvoor je een notificatie buiten het systeem wilt ontvangen.</span>

    <div data-zs-form-template-parser="<[notificationsForm]>">
    </div>

    <script type="text/zs-scope-data">
        {
            "notificationsForm": {
                "name": "notification-settings",
                "actions": [
                    {
                        "name": "submit",
                        "type": "submit",
                        "label": "Opslaan",
                        "importance": "primary",
                        "data": {
                            "url": "/api/user/settings/notifications"
                        }
                    }
                ],
                "fieldsets": [
                    {
                        "name": "notification-setting-fields",
                        "fields": [
                            {
                                "name": "new_assigned_case",
                                "label": "Nieuwe zaak",
                                "type": "checkbox",
                                "value": [% settings.new_assigned_case ? "true": "null" %]
                            },
                            {
                                "name": "new_document",
                                "label": "Nieuw document",
                                "type": "checkbox",
                                "value": [% settings.new_document ? "true" : "null" %]
                            },
                            {
                                "name": "new_attribute_proposal",
                                "label": "Kenmerkwijziging",
                                "type": "checkbox",
                                "value": [% settings.new_attribute_proposal ? "true" : "null" %]
                            },
                            {
                                "name": "new_ext_pip_message",
                                "label": "Bericht vanuit PIP",
                                "type": "checkbox",
                                "value": [% settings.new_ext_pip_message ? "true" : "null" %]
                            },
                            {
                                "name": "case_term_exceeded",
                                "label": "Overschrijden behandeltermijn zaak",
                                "type": "checkbox",
                                "value": [% settings.case_term_exceeded ? "true" : "null" %]
                            },
                            {
                                "name": "case_suspension_term_exceeded",
                                "label": "Overschrijden opschortingstermijn zaak",
                                "type": "checkbox",
                                "value": [% settings.case_suspension_term_exceeded ? "true" : "null" %]
                            }
                        ]
                    }
                ]
            }
        }
    </script>
</div>
[% END %]

[% IF user_can_change_extension %]
<div data-ng-controller="nl.mintlab.user.UserExtensionController" class="contact-telefonie-wijzigen contact-settings-block">
	<h3>Telefoonkoppeling</h3>
		
	<div data-zs-form-template-parser="<[extensionForm]>">
	</div>
	
	<script type="text/zs-scope-data">
		{
			"extensionForm": {
				"name": "changeext",
				"actions": [ 
					{
						"name": "submit",
						"type": "submit",
						"label": "Opslaan",
						"importance": "primary",
						"data": {
							"url": "/api/kcc/user/extension"
						}
					}
				],
				"fieldsets": [
					{
						"name": "ext-change",
						"fields": [
							{
								"name": "extension",
								"label": "Extensie",
								"type": "text",
								"required": true,
                                "value": "<[extension.value]>"
							}
						]
					}
				]
			},
            "extension": [% kcc_extension || 'null' %]
		}
	</script>
</div>
[% END %]

[% PROCESS betrokkene/signature.tt %]

