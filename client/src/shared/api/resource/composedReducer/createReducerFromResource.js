// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import AsyncReducer from './../resourceReducer/AsyncReducer';

export default (resource) => {
  let reducer = new AsyncReducer(),
    remove;

  // TODO: necessary as long as resources call their reducers
  // when they are still pending. remove it when they don't
  reducer.$state = resource.state();
  reducer.setSrc(resource.data());

  remove = resource.onUpdate(() => {
    if (resource.state() === 'resolved') {
      reducer.$resolve(resource.data());
    } else if (resource.state() === 'rejected') {
      reducer.$reject(resource.error());
    }
  });

  resource.onStateChange((state) => {
    reducer.$setState(state);
  });

  reducer.onDestroy(() => {
    remove();
  });

  return reducer;
};
