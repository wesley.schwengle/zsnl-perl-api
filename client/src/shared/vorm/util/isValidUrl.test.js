// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import isValidUrl from './isValidUrl';

describe('The `isValidUrl` function', () => {
  test('accepts just the "file"-protocol', () => {
    expect(isValidUrl('file://')).toBe(true);
  });

  test('does not accept just the protocol when it\'s not the "file"-protocol', () => {
    expect(isValidUrl('test://')).toBe(false);
  });

  test('accepts a url without a protocol', () => {
    expect(isValidUrl('www.zaaksysteem.nl')).toBe(true);
  });

  test('accepts a url without a protocol or subdomain', () => {
    expect(isValidUrl('zaaksysteem.nl')).toBe(true);
  });

  test('accepts a url with a protocol and without a subdomain', () => {
    expect(isValidUrl('https://zaaksysteem.nl')).toBe(true);
  });

  test('accepts a url with a protocol and with a subdomain', () => {
    expect(isValidUrl('https://www.zaaksysteem.nl')).toBe(true);
  });

  test('accepts a url with digits, dots and dashes in the protocol', () => {
    expect(isValidUrl('htt-3.14s://www.zaaksysteem.nl')).toBe(true);
  });

  test('does not accept a url whose protocol does not start with a letter', () => {
    [
      '-https://zaaksysteem',
      '3https://zaaksysteem',
      '.https://zaaksysteem',
    ].forEach((value) => {
      expect(isValidUrl(value)).toBe(false);
    });
  });

  test('accepts a url to just a top-level domain', () => {
    ['zaaksysteem', 'https://zaaksysteem'].forEach((value) => {
      expect(isValidUrl(value)).toBe(true);
    });
  });

  test('accepts an internal url', () => {
    expect(isValidUrl('c:\\windows\\is\\awesome')).toBe(true);
  });

  test('accepts an internal url where the drive is capitalised', () => {
    expect(isValidUrl('C:\\windows\\is\\awesome')).toBe(true);
  });

  test('accepts an internal url to just a drive', () => {
    expect(isValidUrl('c:\\')).toBe(true);
  });

  test('does not accept an interal url when its not prefixed by a drive reference', () => {
    ['1:\\', 'cd:\\', 'windows\\is\\awesome'].forEach((value) => {
      expect(isValidUrl(value)).toBe(false);
    });
  });

  test('does not accept a url where the domain ends with a dot', () => {
    ['zaaksysteem.', 'zaaksysteem.nl.'].forEach((value) => {
      expect(isValidUrl(value)).toBe(false);
    });
  });

  test('accept a url where the path ends with a dot', () => {
    expect(isValidUrl('zaaksysteem.nl/team.')).toBe(true);
  });

  test('accept a url where the domain ends with a slash', () => {
    expect(isValidUrl('zaaksysteem.nl/')).toBe(true);
  });

  test('accept a url where the path ends with a slash', () => {
    expect(isValidUrl('zaaksysteem.nl/team/')).toBe(true);
  });

  test('accept a url with any of its letters capitalised', () => {
    expect(isValidUrl('hTtPs://wWw.ZaAkSyStEeM.nL/TeAm/')).toBe(true);
  });

  test('accept a url with digits, underscores and dashes in the domain', () => {
    expect(isValidUrl('zaak-6_teem.nl/team.')).toBe(true);
  });

  test('accept a url with digits, underscores and dashes in the path', () => {
    expect(isValidUrl('zaaksysteem.nl/te-6_am.')).toBe(true);
  });

  test('does not accept a url with special characters in the domain', () => {
    ['zaak%teem.nl/team', 'zaak?teem.nl/team', 'zaak$teem.nl/team'].forEach(
      (value) => {
        expect(isValidUrl(value)).toBe(false);
      }
    );
  });

  test('accepts a url with special characters in the path', () => {
    expect(isValidUrl('zaaksysteem.nl/te%?$am.')).toBe(true);
  });
});
