// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const fetch = require('node-fetch');
const cspDefaults = require('./csp.json');
const bucketHost = process.env.BUCKET_HOST;
const hostDict = new Map();

setInterval(() => {
  hostDict.clear();
}, 10 * 60 * 1000);

module.exports = (nonce, unsafe, reqHost) => {
  const withBucketHost = (xs) => [...xs, bucketHost].filter(Boolean).join(' ');

  const buildCspHeader = (whitelistDomains) =>
    [
      `default-src 'self' ${withBucketHost(
        cspDefaults.whitelistMain
      )} ${whitelistDomains}`,
      `style-src 'self' ${
        unsafe ? "'unsafe-inline'" : `'nonce-${nonce}'`
      } ${cspDefaults.whitelistStyles.join(' ')} ${whitelistDomains}`,
      `frame-src 'self' ${cspDefaults.whitelistFrames.join(
        ' '
      )} ${whitelistDomains}`,
      `img-src 'self' blob: data: ${withBucketHost(
        cspDefaults.whitelistImages
      )} ${whitelistDomains}`,
      "object-src 'none'",
      "frame-ancestors 'self'",
      "require-trusted-types-for 'script'",
      `connect-src 'self' data: ${withBucketHost(
        cspDefaults.whitelistConnect
      )} ${whitelistDomains}`,
      "base-uri 'self'",
    ].join('; ');

  const cspReq =
    hostDict.get(reqHost) ||
    fetch('https://' + reqHost + '/csp')
      .then((rs) => rs.json())
      .then((res) =>
        res.csp_hosts
          .filter((hostname) => hostname !== '*')
          .map((hostname) => 'https://' + hostname)
          .join(' ')
      )
      .catch(() => '');

  hostDict.set(reqHost, cspReq);

  return cspReq.then(buildCspHeader);
};
